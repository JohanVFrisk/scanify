# Scanify
Make pictures of documents taken with a phone camera look like they were scanned with a document scanner, and compress them to save storage space.
Perfect for digitizing receipts.

## TODO
- verify file exists
- crop
- rotate or cage transform
- save to directory
- custom prefix/postfix

terminal command .bash_profile example:
`alias scanify="python3 <path-to-script>/scanify.py"`