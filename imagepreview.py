import tkinter as tk
from tkinter import ttk
import cv2
from PIL import ImageTk, Image

from processimage import process_image
from save import save_image


class ImagePreview:
	def __init__(self, image_path, quality, block_size, c, max_size):
		self.image_path = image_path
		self.quality = quality

		self.root = tk.Tk()
		self.root.title("Image Preview")
		self.root.geometry("1000x800")

		self.control_frame = tk.Frame(self.root)
		self.control_frame.pack()

		self.block_size_label = tk.Label(self.control_frame, text="Block Size:")
		self.block_size_label.grid(row=0, column=0)
		self.block_size_slider = tk.Scale(self.control_frame, from_=2, to=200, orient=tk.HORIZONTAL,
										  command=self.update_image_preview)
		self.block_size_slider.set(block_size)
		self.block_size_slider.grid(row=0, column=1)

		self.c_label = tk.Label(self.control_frame, text="C Constant:")
		self.c_label.grid(row=1, column=0)
		self.c_slider = tk.Scale(self.control_frame, from_=2, to=40, orient=tk.HORIZONTAL,
								 command=self.update_image_preview)
		self.c_slider.set(c)
		self.c_slider.grid(row=1, column=1)

		self.size_label = tk.Label(self.control_frame, text="Max Size:")
		self.size_label.grid(row=2, column=0)
		self.size_slider = tk.Scale(self.control_frame, from_=600, to=1920, orient=tk.HORIZONTAL,
											command=self.update_image_preview)
		self.size_slider.set(max_size)
		self.size_slider.grid(row=2, column=1)

		self.save_button = tk.Button(self.root, text="Save Image", command=self.save_image)
		self.save_button.pack()

		self.scroll_frame = ttk.Frame(self.root)
		self.scroll_frame.pack(fill='both', expand=True)

		self.canvas = tk.Canvas(self.scroll_frame)
		self.canvas.pack(side='left', fill='both', expand=True)

		# Add a scrollbar to the canvas
		self.scrollbar = ttk.Scrollbar(self.scroll_frame, orient='vertical', command=self.canvas.yview)
		self.scrollbar.pack(side='right', fill='y')

		# Add a scrollbar to the root
		self.scrollbar_x = ttk.Scrollbar(self.root, orient='horizontal', command=self.canvas.xview)
		self.scrollbar_x.pack(side='bottom', fill='x')

		# Configure the canvas to use the scrollbars
		self.canvas.configure(yscrollcommand=self.scrollbar.set, xscrollcommand=self.scrollbar_x.set)
		self.canvas.bind('<Configure>', self.configure_canvas)

		self.image_frame = ttk.Frame(self.canvas)
		self.canvas.create_window((0, 0), window=self.image_frame, anchor='nw')

		self.image_label = tk.Label(self.image_frame)
		self.image_label.pack()

		self.root.protocol("WM_DELETE_WINDOW", self.on_close)
		self.root.bind("<Escape>", lambda _: self.on_close())

	def configure_canvas(self, _):
		# Update the scrollable region of the canvas
		self.canvas.configure(scrollregion=self.canvas.bbox('all'))

	def process_image_with_settings(self):
		block_size = self.block_size_slider.get()
		c = self.c_slider.get()
		max_size = self.size_slider.get()

		return process_image(self.image_path, block_size, c, max_size)

	def update_image_preview(self, _=None):
		image = self.process_image_with_settings()

		# Convert image to RGB for displaying in tkinter
		image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
		image_pil = Image.fromarray(image_rgb)
		image_tk = ImageTk.PhotoImage(image_pil)

		self.image_label.configure(image=image_tk)
		self.image_label.image = image_tk

	def save_image(self):
		image = self.process_image_with_settings()
		save_image(image, self.image_path, self.quality)

		self.on_close()

	def on_close(self):
		self.root.destroy()
