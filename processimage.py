import cv2


def process_image(image_path, block_size, c, max_size):
	image = None

	if image_path.lower().endswith('.heic'):
		from PIL import Image
		from pillow_heif import register_heif_opener
		import numpy as np

		register_heif_opener()

		image = Image.open(image_path)
		image = cv2.cvtColor(np.array(image), cv2.COLOR_RGB2BGR)
	else:
		image = cv2.imread(image_path)

	# Enhance contrast
	## Convert image to grayscale
	gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

	## Apply adaptive thresholding to enhance contrast
	image = cv2.adaptiveThreshold(
		gray,
		255,
		cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
		cv2.THRESH_BINARY,
		block_size // 2 * 2 + 1,
		c)

	## Convert back to BGR
	image = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)

	# Resize image if it exceeds the maximum size
	height, width = image.shape[:2]
	if width > max_size or height > max_size:
		if width > height:
			ratio = max_size / width
		else:
			ratio = max_size / height
		image = cv2.resize(image, (int(width * ratio), int(height * ratio)), interpolation=cv2.INTER_AREA)

	return image
