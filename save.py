import os
import cv2


def save_image(image, input_filename, quality=95):
	output_filename = os.path.splitext(os.path.basename(input_filename))[0] + '_scan.jpg'
	cv2.imwrite(output_filename, image, [cv2.IMWRITE_JPEG_QUALITY, quality])
