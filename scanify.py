import argparse

from imagepreview import ImagePreview
from processimage import process_image
from save import save_image


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Enhance contrast and compress an image.')

	parser.add_argument('input', metavar='input', type=str, nargs='+',
						help='input file(s)')
	parser.add_argument('-b', '--blocksize', metavar='blocksize', type=int, default=110,
						help='block size')
	parser.add_argument('-c', '--constant', metavar='constant', type=int, default=25,
						help='constant')
	parser.add_argument('-q', '--quality', metavar='quality', type=int, default=70,
						help='JPEG quality (0-100, default: 70)')
	parser.add_argument('-s', '--size', metavar='size', type=int, default=1200,
						help='maximum size for resizing (default: 1200)')
	parser.add_argument('-g', '--gui', action='store_true',
						help='show gui wth preview')

	args = parser.parse_args()

	for input_file in args.input:

		if args.gui:
			# Create and show the image preview GUI in a separate window
			gui = ImagePreview(input_file, args.quality, args.blocksize, args.constant, args.size)
			gui.update_image_preview()
			gui.root.mainloop()

		else:
			# Command line tool
			img = process_image(input_file, args.blocksize, args.constant, args.size)
			save_image(img, input_file, args.quality)
